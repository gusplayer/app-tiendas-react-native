import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import AuthLoadingScreen from '../navigation/AuthLoadingScreen';
import {DashNavigator, LoginNavigator} from './Router';

const AppStack = createStackNavigator({DashNavigator});
const AuthStack = createStackNavigator({LoginNavigator});

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    },
  ),
);
