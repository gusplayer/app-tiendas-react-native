import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  SafeAreaView,
  Touchable
} from "react-native";
import TabBar from "../../src/components/tabBar";
import { Colors } from "../../utils/const";
import {
  Container,
  Tab,
  Tabs,
  TabHeading,
  Header,
  Left,
  Button,
  Right,
  Body,
  Title
} from "native-base";
import Profile from "./Profile";
import Store from "./Store";
import Membership from "./Membership";
import Icon from "react-native-vector-icons/Feather";
import Head from "../../src/components/header";

export default class Membreship extends React.Component {
  constructor() {
    super();
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Container>
          <Head label="Ajustes" />

          <ScrollView style={styles.body}>
            <Tabs tabBarUnderlineStyle={{ backgroundColor: Colors.first }}>
              <Tab
                heading={
                  <TabHeading style={{ backgroundColor: "white" }}>
                    <Text style={{ color: "black" }}>Mi Tienda</Text>
                  </TabHeading>
                }
              >
                <Store navigation={this.props.navigation} />
              </Tab>
              <Tab
                heading={
                  <TabHeading style={{ backgroundColor: "white" }}>
                    <Text style={{ color: "black" }}>Mi Perfil</Text>
                  </TabHeading>
                }
              >
                <Profile navigation={this.props.navigation} />
              </Tab>
              <Tab
                heading={
                  <TabHeading style={{ backgroundColor: "white" }}>
                    <Text style={{ color: "black" }}>Suscripción</Text>
                  </TabHeading>
                }
              >
                <Membership navigation={this.props.navigation} />
              </Tab>
            </Tabs>
          </ScrollView>
          <TabBar navigation={this.props.navigation} position={4} />
        </Container>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "space-around"
  },
  imageTop: {
    width: "100%",
    height: 100
  }
});
