import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  ScrollView,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {Text, Button, Container} from 'native-base';
import Icon from 'react-native-vector-icons/Feather';
import API from '../../utils/api';
import LottieView from 'lottie-react-native';
import {SalesState, formatCurrency, Colors, Payments} from '../../utils/const';
import FastImage from 'react-native-fast-image';
import moment from 'moment';
import 'moment/locale/es';
import Modal from 'react-native-modal';

export default class NewSaleProductDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      products: '',
      infoSale: '',
      tiendaName: '',
      direccionEntrega: '',
      tipoEntrega: '',
      costo_envio: '',
      userIdentification: '',
      cityWithName: false,
      cityName: '',
      depName: '',
      loading: true,
      isModalVisible: false,
      stateSale: 0,
      changeState: false,
      showToast: false,
    };
  }

  async componentDidMount() {
    const props = this.props.navigation.state.params.sale;
    const infoSale = await API.getSaleDetails(props.id);
    const cities = await API.getCities();

    this.compareTypeShipping(infoSale.venta.direccion_entrega);
    this.verifyCityName(infoSale.venta.direccion_entrega, cities);
    this.setState({
      infoSale: infoSale,
      tiendaName: infoSale.venta.tienda_venta.nombre,
      costo_envio: infoSale.venta.costo_envio,
      userIdentification: infoSale.venta.usuario.identificacion,
      products: infoSale.productos,
      stateSale: infoSale.venta.estado,
      loading: false,
    });
  }

  // Compara si ciudad viene con nombre-ciudad

  verifyCityName(deliveryData, cities) {
    if (typeof deliveryData === 'string' && deliveryData != '0') {
      deliveryData = JSON.parse(deliveryData);

      // Es un objeto con la informacion de entrega, no es un numero
      if (deliveryData.hasOwnProperty('value')) {
        // No tiene ciudad
        if (!deliveryData.value.hasOwnProperty('ciudad')) {
          const city = cities.find(
            city => city.id === deliveryData.value.ciudad_id,
          );
          this.setState({
            cityName: city.nombre_ciu,
            depName: city.departamento.nombre_dep,
          });
        }
        //viene con ciudad
        else {
          this.setState({
            cityWithName: true,
          });
        }
      }
    }
    //Viene en cero la direccion de entrega
    else {
    }
  }

  modal() {
    const {navigate} = this.props.navigation;
    const props = this.props.navigation.state.params.sale;

    return (
      <View style={styles.modalContent}>
        <View
          style={{
            width: '100%',
            height: '100%',
            flex: 3,
            padding: 16,
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            onPress={() => {
              this.setState({isModalVisible: false});
            }}
            style={{
              flexDirection: 'row',
              width: '100%',
              height: 10,
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}>
            <Text
              onPress={() => {
                this.setState({isModalVisible: false});
              }}>
              {' '}
              <Icon name="x-circle" size={26} color="#FF7F50" />{' '}
            </Text>
          </View>
          <Text
            style={{
              color: 'black',
              fontSize: 16,
              alignItems: 'center',
              padding: 5,
              fontWeight: '600',
              paddingHorizontal: 10,
            }}>
            Estado actual:
          </Text>

          <View style={{marginLeft: -2, marginBottom: 10}}>
            {SalesState(this.state.stateSale)}
          </View>

          <Text
            style={{
              color: 'black',
              fontSize: 16,
              alignItems: 'center',
              padding: 5,
              fontWeight: '600',
              paddingHorizontal: 10,
            }}>
            Cambiar por:
          </Text>
          <TouchableOpacity
            onPress={async () => {
              let estado = '0';
              await API.putUpdateSale(props.id, estado);
              this.setState({
                isModalVisible: false,
                stateSale: estado,
                changeState: true,
              });
            }}
            style={styles.buttonUpdateState}>
            <Text style={styles.changeStateText}> Sin pagar </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={async () => {
              let estado = '3';
              await API.putUpdateSale(props.id, estado);
              this.setState({
                isModalVisible: false,
                stateSale: estado,
                changeState: true,
              });
            }}
            style={styles.buttonUpdateState}>
            <Text style={styles.changeStateText}> Cancelada</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={async () => {
              let estado = '1';
              await API.putUpdateSale(props.id, estado);
              this.setState({
                isModalVisible: false,
                stateSale: estado,
                changeState: true,
              });
            }}
            style={styles.buttonUpdateState}>
            <Text style={styles.changeStateText}> Pagada </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={async () => {
              let estado = '4';
              await API.putUpdateSale(props.id, estado);
              this.setState({
                isModalVisible: false,
                stateSale: estado,
                changeState: true,
              });
            }}
            style={styles.buttonUpdateState}>
            <Text style={styles.changeStateText}> Despachado </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={async () => {
              let estado = '6';
              await API.putUpdateSale(props.id, estado);
              this.setState({
                isModalVisible: false,
                stateSale: estado,
                changeState: true,
              });
            }}
            style={styles.buttonUpdateState}>
            <Text style={styles.changeStateText}> Entregado </Text>
          </TouchableOpacity>
          {/* <TouchableOpacity
            onPress={() => {
              this.setState({ isModalVisible: false });
            }}
            style={styles.finishButton}
          >
            <Text style={{ color: "black", marginTop: 15 }}>
              Cerrar esta ventana
            </Text>
          </TouchableOpacity> */}
        </View>
      </View>
    );
  }

  //comparar tipo de direccion de entrega de la venta
  compareTypeShipping(direccionEntrega) {
    if (typeof direccionEntrega === 'string' && direccionEntrega != '0') {
      direccionEntrega = JSON.parse(direccionEntrega);

      // Es un objeto con la informacion de entrega
      if (direccionEntrega.hasOwnProperty('value')) {
        //type 1 = entrega a domicilio
        if (direccionEntrega.type == '1') {
          this.setState({
            tipoEntrega: 1,
            direccionEntrega: direccionEntrega,
          });
        }
        //type 2 = entrega a domicilio
        if (direccionEntrega.type == '2') {
          console.warn('tipo 2');
          this.setState({
            tipoEntrega: 2,
            direccionEntrega: direccionEntrega,
          });
        }
      }
      // No es un objeto y tampoco cero, no tiene envio
      else {
        this.setState({
          tipoEntrega: 99,
        });
      }
      // Si es cero, no tiene envio
    } else {
      this.setState({
        tipoEntrega: 0,
      });
    }
  }

  fitImage(image) {
    let fitImage = image.split('/upload/');
    return (
      'https://res.cloudinary.com/komercia-store/image/upload/w_250,q_auto:best,f_auto/' +
      fitImage[1]
    );
  }

  converterVariants(variants) {
    let formatedVariants = JSON.parse('' + variants + '');
    let joinArray = formatedVariants.join(' / ');

    return joinArray;
  }

  productList() {
    if (this.state.loading) {
      return (
        <View style={styles.containerLoading}>
          <LottieView
            style={{
              justifyContent: 'center',
              backgroundColor: 'white',
            }}
            autoPlay
            loop
            source={require('../../src/animations/bag.json')}
          />
        </View>
      );
    }
    return (
      <FlatList
        style={styles.flatList}
        data={this.state.products}
        keyExtractor={(item, _) => item.producto.nombre}
        renderItem={({item}) => (
          <View style={styles.itemList}>
            <View style={styles.infoProduct}>
              <FastImage
                style={styles.imageProduct}
                resizeMode={FastImage.resizeMode.cover}
                source={{
                  uri: this.fitImage(item.producto.foto_cloudinary),
                }}
              />
              <View style={styles.itemListText}>
                <Text>{item.producto.nombre}</Text>
                <Text style={styles.textQuantity}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: 'bold',
                      color: Colors.first,
                    }}>
                    {item.unidades}{' '}
                  </Text>
                  {item.unidades == 1 ? (
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: 'bold',
                        color: Colors.first,
                      }}>
                      unidad
                    </Text>
                  ) : (
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: 'bold',
                        color: Colors.first,
                      }}>
                      unidades
                    </Text>
                  )}
                  <Text
                    style={{
                      fontSize: 13,
                      fontWeight: 'bold',
                    }}>
                    {' '}
                    x
                  </Text>{' '}
                  $ {formatCurrency(item.precio_producto)}
                </Text>
                {item.producto.con_variante == 1 && (
                  <View
                    style={{
                      backgroundColor: Colors.first,
                      flexDirection: 'row',
                      marginTop: 5,
                      padding: 5,
                      alignItems: 'center',
                      justifyContent: 'flex-start',
                      alignSelf: 'flex-start',
                      borderColor: Colors.first,
                      borderRadius: 4,
                      borderWidth: 0.5,
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 12,
                      }}>
                      {this.converterVariants(item.variantes)}
                    </Text>
                  </View>
                )}
              </View>
              <View style={styles.itemListPrice}>
                <Text style={styles.priceProduct}>
                  $ {formatCurrency(item.precio_producto * item.unidades)}
                </Text>
              </View>
            </View>
            <View style={styles.separator} />
          </View>
        )}
      />
    );
  }

  render() {
    const {navigate} = this.props.navigation;
    const {goBack} = this.props.navigation;
    const props = this.props.navigation.state.params.sale;

    return (
      <SafeAreaView style={{flex: 1}}>
        <Modal isVisible={this.state.isModalVisible}>{this.modal()}</Modal>
        {/* {this.toast()} */}
        <Container>
          <ScrollView>
            {this.state.changeState ? (
              <View style={styles.header}>
                <Button transparent onPress={() => navigate('Sales')}>
                  <Icon name="arrow-left" color="black" size={30} />
                </Button>
              </View>
            ) : (
              <View style={styles.header}>
                <Button transparent onPress={() => goBack()}>
                  <Icon name="arrow-left" color="black" size={30} />
                </Button>
              </View>
            )}

            <View style={styles.productDetails}>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.title}>Información venta</Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: 230,
                  marginBottom: 10,
                  marginTop: 5,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Text style={styles.nameProduct}>ID : </Text>
                  <Text style={styles.nameProduct}> {props.id}</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Text style={styles.nameProduct}>Fecha : </Text>
                  <Text style={styles.nameProduct}>{props.fecha}</Text>
                </View>
              </View>

              {this.productList()}
            </View>

            <View style={styles.costumerDetails}>
              <View>
                <Text style={styles.title}>Detalles de la venta</Text>

                <View
                  onPress={() => {
                    this.setState({
                      isModalVisible: true,
                    });
                  }}
                  style={{
                    marginLeft: -2,
                    marginBottom: 10,
                    flexDirection: 'row',
                    alignItems: 'center',
                    alignContent: 'center',
                  }}>
                  {SalesState(this.state.stateSale)}
                  <Text
                    onPress={() => {
                      this.setState({
                        isModalVisible: true,
                      });
                    }}
                    style={{
                      borderRadius: 15,
                      padding: 4,
                      marginLeft: 11,
                      fontWeight: 'bold',
                      fontSize: 12,
                      alignItems: 'center',
                      justifyContent: 'center',
                      color: Colors.first,
                    }}>
                    <Icon name="edit" size={16} color={Colors.first} /> editar
                  </Text>
                </View>

                <Text style={styles.nameProduct}>Nombre</Text>
                <Text>{props.usuario.nombre}</Text>

                <Text style={styles.nameProduct}>Fecha venta</Text>
                <Text>
                  {/* {moment(props.created_at).format("MMMM Do YYYY, h:mm:ss a")} */}
                  {moment(props.created_at).format('MMMM D, h:mm:ss a')}
                </Text>

                <Text style={styles.nameProduct}>Email</Text>
                <Text>{props.usuario.email}</Text>
                <Text style={styles.nameProduct}>Identificación Comprador</Text>
                <Text>{this.state.userIdentification}</Text>

                <Text style={styles.nameProduct}>Medio de pago</Text>
                {Payments(props.metodo_pago)}
              </View>
            </View>

            {this.state.tipoEntrega == 1 && this.state.cityWithName == true && (
              <View style={styles.shippingDetails}>
                <View>
                  <Text style={styles.title}>Información de envío</Text>
                  <Text style={styles.nameProduct}>Persona que recibe</Text>
                  <Text>{this.state.direccionEntrega.value.nombre}</Text>
                  <Text style={styles.nameProduct}>Telefono</Text>
                  <Text>{this.state.direccionEntrega.value.celular}</Text>
                  <Text style={styles.nameProduct}>Dirección</Text>
                  <Text>{this.state.direccionEntrega.value.direccion}</Text>
                  <Text style={styles.nameProduct}>Barrio</Text>
                  <Text>{this.state.direccionEntrega.value.barrio}</Text>

                  <Text style={styles.nameProduct}>Ciudad</Text>
                  <Text>
                    {this.state.direccionEntrega.value.ciudad.nombre_ciu}
                  </Text>
                  <Text style={styles.nameProduct}>Departamento</Text>
                  <Text>
                    {
                      this.state.direccionEntrega.value.ciudad.departamento
                        .nombre_dep
                    }
                  </Text>

                  <Text style={styles.nameProduct}>Valor del envío</Text>
                  <Text>$ {formatCurrency(this.state.costo_envio)}</Text>
                </View>
              </View>
            )}

            {this.state.tipoEntrega == 1 && this.state.cityWithName == false && (
              <View style={styles.shippingDetails}>
                <View>
                  <Text style={styles.title}>Información de envío</Text>
                  <Text style={styles.nameProduct}>Persona que recibe</Text>
                  <Text>{this.state.direccionEntrega.value.nombre}</Text>
                  <Text style={styles.nameProduct}>Telefono</Text>
                  <Text>{this.state.direccionEntrega.value.celular}</Text>
                  <Text style={styles.nameProduct}>Dirección</Text>
                  <Text>{this.state.direccionEntrega.value.direccion}</Text>
                  <Text style={styles.nameProduct}>Barrio</Text>
                  <Text>{this.state.direccionEntrega.value.barrio}</Text>

                  <Text style={styles.nameProduct}>Ciudad</Text>
                  <Text>{this.state.cityName}</Text>
                  <Text style={styles.nameProduct}>Departamento</Text>
                  <Text>{this.state.depName}</Text>

                  <Text style={styles.nameProduct}>Valor del envío</Text>
                  <Text>$ {formatCurrency(this.state.costo_envio)}</Text>
                </View>
              </View>
            )}

            {this.state.tipoEntrega == 0 && (
              <View style={styles.shippingDetails}>
                <View>
                  <Text style={styles.nameProduct}>
                    No hay información de envio
                  </Text>
                </View>
              </View>
            )}

            {this.state.tipoEntrega == 99 && (
              <View style={styles.shippingDetails}>
                <View>
                  <Text style={styles.nameProduct}>
                    No hay información de envio
                  </Text>
                </View>
              </View>
            )}
          </ScrollView>
          {/* <View style={styles.contentChangeStateSale}>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  isModalVisible: true
                });
              }}
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignSelf: "center",
                borderRadius: 5,
                borderWidth: 0.5,
                borderColor: Colors.first,
                padding: 8
              }}
            >
              <Text style={styles.changeStateText}> Actualizar </Text>
            </TouchableOpacity>
          </View> */}

          <View style={styles.contentTotal}>
            <View style={styles.sectionTotal}>
              <Text style={styles.textTotal}>Total Venta</Text>
              <Text style={styles.priceTotal}>
                $ {formatCurrency(props.total)}
              </Text>
            </View>
          </View>
        </Container>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  containerLoading: {
    height: 110,
    width: '100%',
    marginVertical: 40,
    flexDirection: 'column',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignContent: 'center',
  },
  header: {
    backgroundColor: 'white',
    height: 60,
    paddingHorizontal: 20,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  productDetails: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    width: '100%',
    padding: 20,
    paddingTop: 5,
  },
  salesDetails: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    width: '100%',
    padding: 20,
    paddingTop: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  costumerDetails: {
    flex: 1,
    backgroundColor: '#F1F1F2',
    width: '100%',
    padding: 20,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  shippingDetails: {
    flex: 1,
    backgroundColor: '#ffffff',
    width: '100%',
    padding: 20,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  detailsLeft: {
    marginRight: 15,
    alignItems: 'center',
  },
  contentTotal: {
    width: '100%',
    height: 50,
    backgroundColor: Colors.first,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#E5E5E5',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },

  priceTotal: {
    fontSize: 22,
    color: 'white',
    letterSpacing: 0.16,
  },
  textTotal: {
    color: 'white',
    fontSize: 15,
  },
  itemList: {
    width: '95%',
    flexDirection: 'column',
    marginVertical: 15,
  },
  infoProduct: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    width: '100%',
  },
  itemListText: {
    marginLeft: 8,
    justifyContent: 'center',
    width: '53%',
  },
  itemListPrice: {
    marginLeft: 8,
    justifyContent: 'center',
    width: '33%',
  },
  nameProduct: {
    fontWeight: '400',
    color: '#7f8c8d',
    fontSize: 13,
  },
  textQuantity: {
    fontWeight: '400',
    color: '#7f8c8d',
    fontSize: 13,
    // color: "#4834d4"
  },
  textPhotoProfile: {
    marginTop: 5,
    fontWeight: '400',
    fontSize: 12,
    color: '#4834d4',
  },
  imageProfile: {
    width: 70,
    height: 70,
    borderRadius: 45,
  },
  imageProduct: {
    width: 60,
    height: 60,
    borderRadius: 6,
    marginRight: 5,
  },
  title: {
    fontSize: 20,
    color: 'black',
    fontWeight: '900',
    marginBottom: 10,
  },
  separator: {
    backgroundColor: '#ecf0f1',
    height: 1,
    width: '100%',
    marginVertical: 5,
  },
  contentChangeStateSale: {
    width: '100%',
    height: 50,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#E5E5E5',
    paddingHorizontal: 20,
    elevation: 5,
  },
  changeStateText: {
    color: Colors.first,
    fontWeight: '600',
  },
  sectionTotal: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  modalContent: {
    flexDirection: 'column',
    borderRadius: 8,
    height: 410,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    overflow: 'hidden',
    backgroundColor: 'white',
  },
  buttonLogin: {
    width: '72%',
    backgroundColor: Colors.first,
    height: 38,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 14,
    letterSpacing: 0.13,
    fontWeight: '800',
  },
  buttonUpdateState: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: Colors.first,
    padding: 8,
    width: 130,
  },
});
