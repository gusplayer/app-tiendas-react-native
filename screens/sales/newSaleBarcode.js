import React from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ActivityIndicator,
  TextInput,
  Image
} from "react-native";
import { Text } from "native-base";
import { RNCamera } from "react-native-camera";
import API from "../../utils/api";

export default class NewSaleBarcode extends React.Component {
  constructor() {
    super();
    this.state = { loading: false };
  }
  onBarCodeRead = e => {
    const { navigate } = this.props.navigation;
    this.setState({ loading: true });
    // this.props.navigation.navigate("NewSaleProductDetails", { barcodes: e });
    if (e[0]) {
      this.getProductBarcode(e[0].data);
    } else {
      this.getProductBarcode(e.data);
    }
  };
  async getProductBarcode(barcode) {
    let detailsProductsAPI = await API.getProductBarCode(barcode);
    if (detailsProductsAPI == null) {
      console.warn("El producto no existe");
    } else {
      this.props.navigation.navigate("DetailsProducts", {
        product: detailsProductsAPI.detalle
      });
    }
    this.setState({ loading: false });
  }

  _viewCamera() {
    if (this.state.loading) {
      return (
        <ActivityIndicator
          style={styles.preview}
          size="large"
          color="#0000ff"
        />
      );
    } else {
      return (
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          onBarCodeRead={this.onBarCodeRead}
          showMarker={true}
          playSoundOnCapture
          permissionDialogTitle={"Permission to use camera"}
          permissionDialogMessage={
            "We need your permission to use your camera phone"
          }
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            this.onBarCodeRead(barcodes);
          }}
        />
      );
    }
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        {this._viewCamera()}

        <View
          style={{ paddingTop: 15 }}
          onPress={() => navigate("NewSaleBarcode")}
        >
          <Image
            style={{
              width: 55,
              height: 41,
              alignSelf: "center"
            }}
            source={require("../../src/assets/barcode2.png")}
          />
        </View>

        {/* <View style={styles.separator}>
          <Text style={{ fontSize: 12, marginBottom: 2 }}>
            Si el producto no tiene codigo de barras
          </Text>
          <Text>Escribe el nombre del producto</Text>
        </View>
        <View style={styles.footer}>
          <TextInput
            placeholderTextColor="#59617b"
            placeholder={"Nombre del producto"}
            style={styles.input}
            underlineColorAndroid="rgba(0,0,0,0)"
            onChangeText={email => this.setState({ email })}
          />
          <TouchableOpacity
            onPress={() => navigate("NewSaleProductDetails")}
            style={styles.capture}
          >
            <Text style={{ color: "white" }}>Buscar</Text>
          </TouchableOpacity>
        </View> */}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  lineStyle: {
    borderWidth: 0.5,
    borderColor: "black",
    margin: 10
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column"
  },
  scopeCamera: {
    width: 300,
    height: 200
  },
  preview: {
    width: 300,
    height: 200,
    backgroundColor: "white",
    borderRadius: 10
  },

  input: {
    backgroundColor: "#F1F1F2",
    height: 47,
    width: 215,
    paddingLeft: 20,
    borderRadius: 2,
    borderColor: "#303456",
    borderWidth: 1,
    color: "#59617b",
    fontWeight: "600",
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowColor: "#303456",
    shadowOffset: { width: 2, height: 2 }
  }
});
