import React from "react";
import {
  StyleSheet,
  ActivityIndicator,
  Text,
  Image,
  View,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  FlatList,
  SafeAreaView
} from "react-native";
import HTML from "react-native-render-html";
import { Container, Button, Badge } from "native-base";
import API from "../../utils/api";
import Icon from "react-native-vector-icons/Feather";
import Swiper from "react-native-swiper";
import { connect } from "react-redux";
import { Colors, formatCurrency } from "../../utils/const";
import FastImage from "react-native-fast-image";
import Share, { ShareSheet } from "react-native-share";
import RNFetchBlob from "rn-fetch-blob";
const win = Dimensions.get("window");
class DetailsProducts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productDetails: "",
      loading: true,
      visible: false,
      imageShare: "",
      cantidad: 1,
      value: 1
    };
  }

  onCancel() {
    console.log("CANCEL");
    this.setState({ visible: false });
  }
  onOpen() {
    console.log("OPEN");
    this.setState({ visible: true });
  }

  header() {
    const { goBack } = this.props.navigation;
    const { navigate } = this.props.navigation;
    const item = this.props.navigation.state.params.product;
    let shareImageBase64 = {
      title: item.nombre,
      message: item.nombre,
      url: this.state.imageShare,
      type: "image/png",
      subject: "Share Link" //  for email
    };

    return (
      <View style={styles.header}>
        <Button transparent onPress={() => goBack()}>
          <Icon name="arrow-left" color="black" size={30} />
        </Button>
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            style={styles.tabShare}
            onPress={() => {
              Share.open(shareImageBase64);
            }}
          >
            <Icon name="share-2" style={styles.tabCartIcon} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.tabCart}
            // onPress={this.props.addItemToCart}
            onPress={() => navigate("NewSaleGeneralDetails")}
          >
            <Icon name="shopping-cart" style={styles.tabCartIcon} />

            {this.props.cartItems.length > 0 ? (
              <Badge
                style={{
                  width: 20,
                  height: 20,
                  position: "absolute",
                  backgroundColor: "#0000ff",
                  marginLeft: 34
                }}
              >
                <Text style={{ color: "#fff" }}>
                  {this.props.cartItems.length}
                </Text>
              </Badge>
            ) : (
              <View />
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  base64Image = () => {
    const item = this.props.navigation.state.params.product;
    const fs = RNFetchBlob.fs;
    let shareImageBase64 = {
      title: item.nombre,
      message: item.nombre,
      url: this.state.imageShare,
      type: "image/png",
      subject: "Share Link" //  for email
    };
    let imagePath = null;
    RNFetchBlob.config({
      fileCache: true
    })
      .fetch("GET", item.foto_cloudinary)
      // the image is now dowloaded to device's storage
      .then(resp => {
        // the image path you can use it directly with Image component
        imagePath = resp.path();
        return resp.readFile("base64");
      })
      .then(base64Data => {
        // here's base64 encoded image
        this.setState({ imageShare: `data:image/png;base64,${base64Data}` });
        return `data:image/png;base64,${base64Data}`;
        // remove the file from storage
        return fs.unlink(imagePath);
      });
  };

  async componentDidMount() {
    let detailsProductsAPI = await API.getProductDetails(
      this.props.navigation.state.params.product.id
    );
    this.setState({
      productDetails: detailsProductsAPI,
      loading: false
    });
    this.base64Image();
  }

  fitImage(image) {
    let fitImage = image.split("/upload/");
    return (
      "https://res.cloudinary.com/komercia-store/image/upload/w_650,f_auto,fl_progressive:steep/" +
      fitImage[1]
    );
  }

  // _getButtonShare() {
  //   const item = this.props.navigation.state.params.product;
  //   let shareImageBase64 = {
  //     title: item.nombre,
  //     message: item.nombre,
  //     url: this.state.imageShare,
  //     type: "image/png",
  //     subject: "Share Link" //  for email
  //   };

  //   const fs = RNFetchBlob.fs;
  //   return (
  //     <TouchableOpacity
  //       onPress={() => {
  //         Share.open(shareImageBase64);
  //       }}
  //       style={styles.buttonLogin}
  //     >
  //       <View style={styles.alignButton}>
  //         <Text style={styles.buttonText}>Compartir</Text>
  //       </View>
  //     </TouchableOpacity>
  //   );
  // }

  _getButtonShare() {
    const item = this.props.navigation.state.params.product;
    let shareImageBase64 = {
      title: item.nombre,
      message:
        "Producto: " +
        item.nombre +
        " , Comprar: " +
        item.tienda +
        ".komercia.co/productos/" +
        item.slug,
      url: this.state.imageShare,
      type: "image/png",
      subject: "Share Link" //  for email
    };

    const fs = RNFetchBlob.fs;
    return (
      <TouchableOpacity
        onPress={() => {
          Share.open(shareImageBase64);
        }}
        style={styles.buttonLogin}
      >
        <View style={styles.alignButton}>
          <Text style={styles.buttonText}>Compartir</Text>
        </View>
      </TouchableOpacity>
    );
  }

  showImages1 = () => {
    if (this.state.loading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" color="#f14b5a" />
          <Text style={{ textAlign: "center" }}>Cargando Información</Text>
        </View>
      );
    } else {
      if (this.state.productDetails.fotos[0]) {
        return (
          <View style={styles.slide1}>
            {this.state.productDetails.fotos[0] && (
              <FastImage
                style={styles.imageProduct}
                resizeMode="contain"
                source={{
                  uri: this.fitImage(
                    this.state.productDetails.fotos[0].foto_cloudinary
                  )
                }}
              />
            )}
          </View>
        );
      }
    }
  };

  showImages2 = () => {
    if (this.state.loading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" color="#f14b5a" />
          <Text style={{ textAlign: "center" }}>Cargando Información</Text>
        </View>
      );
    } else {
      if (this.state.productDetails.fotos[1]) {
        return (
          <View style={styles.slide1}>
            {this.state.productDetails.fotos[1] && (
              <Image
                style={styles.imageProduct}
                resizeMode="contain"
                source={{
                  uri: this.fitImage(
                    this.state.productDetails.fotos[1].foto_cloudinary
                  )
                }}
              />
            )}
          </View>
        );
      } else {
        <View />;
      }
    }
  };

  variantes = variantes => {
    variantes = JSON.parse(variantes);

    return (
      <View>
        <Text style={styles.title}>Variantes</Text>
        <FlatList
          showsHorizontalScrollIndicator={false}
          style={{ width: "100%", marginTop: 10, marginBottom: 10 }}
          horizontal={false}
          data={variantes}
          keyExtractor={(item, _) => item.combinacion}
          renderItem={({ item }) => (
            <View
              style={{
                flexDirection: "row",
                width: "100%",
                padding: 10,
                alignItems: "center",
                borderRadius: 3,
                justifyContent: "space-between",
                backgroundColor: "white",
                marginBottom: 8
              }}
            >
              <View style={{ flex: 3 }}>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5,
                    flexWrap: "wrap"
                  }}
                >
                  <View
                    style={{
                      backgroundColor: Colors.second,
                      paddingHorizontal: 8,
                      color: "white",
                      borderRadius: 4,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Text
                      style={{
                        color: "white",
                        fontWeight: "bold"
                      }}
                    >
                      {item.combinacion[0]}
                    </Text>
                  </View>

                  {item.combinacion[1] && (
                    <Text style={{ color: "black", fontWeight: "bold" }}>
                      {" "}
                      +{" "}
                    </Text>
                  )}

                  {item.combinacion[1] && (
                    <View
                      style={{
                        backgroundColor: Colors.morado_claro,
                        paddingHorizontal: 8,
                        color: "white",
                        borderRadius: 4,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontWeight: "bold"
                        }}
                      >
                        {item.combinacion[1]}
                      </Text>
                    </View>
                  )}

                  {item.combinacion[2] && (
                    <Text style={{ color: "black", fontWeight: "bold" }}>
                      {" "}
                      +{" "}
                    </Text>
                  )}

                  {item.combinacion[2] && (
                    <View
                      style={{
                        backgroundColor: "gray",
                        paddingHorizontal: 8,
                        color: "white",
                        borderRadius: 4,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontWeight: "bold"
                        }}
                      >
                        {item.combinacion[2]}
                      </Text>
                    </View>
                  )}
                </View>
                <Text style={{ color: Colors.first }}>
                  <Text style={{ fontWeight: "bold" }}>{item.unidades} </Text>
                  Unidades disponibles
                </Text>
              </View>
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 15,
                  flex: 1,
                  textAlign: "right"
                }}
              >
                $ {formatCurrency(item.precio)}
              </Text>
            </View>
          )}
        />
      </View>
    );
  };

  // handleClick = () => {
  //   const item = this.props.navigation.state.params.product;
  //   console.warn(item);
  //   this.props.addItemToCart(item);
  // };

  render() {
    const { navigate } = this.props.navigation;
    const item = this.props.navigation.state.params.product;
    const { goBack } = this.props.navigation;

    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.body}>
          {this.header()}
          <View style={styles.imageContainer}>
            <View style={styles.infoImageProduct}>
              <Swiper
                style={{ flex: 1, width: "100%" }}
                showsButtons={false}
                dot={
                  <View
                    style={{
                      backgroundColor: "rgba(255,255,255,.3)",
                      width: 13,
                      height: 13,
                      borderRadius: 7,
                      marginLeft: 7,
                      marginRight: 7
                    }}
                  />
                }
                activeDot={
                  <View
                    style={{
                      backgroundColor: "#4c4c4c",
                      width: 13,
                      height: 13,
                      borderRadius: 7,
                      marginLeft: 7,
                      marginRight: 7
                    }}
                  />
                }
                paginationStyle={{
                  bottom: 70
                }}
                loop={false}
              >
                <View style={styles.slide1}>
                  <FastImage
                    style={styles.imageProduct}
                    resizeMode={FastImage.resizeMode.cover}
                    source={{ uri: this.fitImage(item.foto_cloudinary) }}
                  />
                </View>
              </Swiper>
            </View>
          </View>

          <View style={styles.contentDetails}>
            <Text style={styles.itemNombre}>{item.nombre.toUpperCase()}</Text>
            <Text style={styles.textStars}>
              <Icon name="star" size={15} color="#f1c40f" />{" "}
              <Icon name="star" size={15} color="#f1c40f" />{" "}
              <Icon name="star" size={15} color="#f1c40f" /> {"  "}
              Calificaciones
            </Text>

            {item.precio > 0 && item.con_variante == 0 && (
              <Text style={styles.priceProduct}>
                $ {formatCurrency(item.precio)}
              </Text>
            )}

            {item.precio == 0 && item.con_variante == 0 && (
              <Text style={styles.priceProduct}>Producto sin precio</Text>
            )}

            {item.precio == 0 && item.con_variante == 1 && (
              <View>
                {this.variantes(
                  item.variantes[0].combinaciones[0].combinaciones
                )}
              </View>
            )}

            {item.precio > 0 && item.con_variante == 0 && (
              <Text>{item.inventario} unidades disponibles</Text>
            )}

            {/* <Text style={{ color: "#27ae60" }}>
              <Icon name="eye" /> {item.visitas} Visualizaciones
            </Text> */}

            {/* <Text style={styles.title}>Descripción</Text>
            <View style={styles.contentDescription}>
              <ScrollView style={{ flex: 1 }}>
                <HTML
                  imagesMaxWidth={Dimensions.get("window").width}
                  style={{ color: "#7f8c8d" }}
                  html={item.descripcion}
                />
              </ScrollView>
            </View> */}
          </View>

          {/* <View style={styles.footer}>
          <NumericInput
            initValue={1}
            totalWidth={120}
            totalHeight={60}
            minValue={1}
            step={1}
            inputStyle={{ fontSize: 17, width: 40 }}
            containerStyle={{ fontSize: 17 }}
            iconStyle={{ fontSize: 16 }}
            iconSize={10}
            onPress={value => this.setState({ cantidad: value })}
          />

          <TouchableOpacity
            style={styles.botonAdd}
            onPress={() => {
              this.props.addItemToCart(item);
            }}
          >
            <Text
              style={{ color: "white", alignItems: "center", fontSize: 14 }}
            >
              AÑADIR A LA VENTA
            </Text>
          </TouchableOpacity>
        </View> */}
          {/* <TouchableOpacity
            style={styles.botonAdd}
            onPress={() => {
              this.props.addItemToCart(item);
            }}
          >
            <Text
              style={{ color: "white", alignItems: "center", fontSize: 14 }}
            >
              AÑADIR A LA VENTA
            </Text>
          </TouchableOpacity> */}
        </ScrollView>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          {this._getButtonShare()}
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    cartItems: state.cartItems
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addItemToCart: id => dispatch({ type: "ADD_TO_CART", payload: id })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailsProducts);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start"
  },
  body: {
    flex: 1
  },
  tabCart: {
    zIndex: 9999,
    backgroundColor: Colors.first,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 40,
    width: 40,
    elevation: 4
  },
  tabCartIcon: {
    color: "white",
    fontSize: 18,
    fontWeight: "100"
  },
  tabShare: {
    zIndex: 9999,
    backgroundColor: Colors.second,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 40,
    width: 40,
    elevation: 6,
    marginRight: 8
  },
  tabShareIcon: {
    color: "white",
    fontSize: 16,
    fontWeight: "100"
  },
  botonAdd: {
    flex: 1,
    height: 10,
    backgroundColor: Colors.first,
    justifyContent: "center",
    alignItems: "center"
  },
  header: {
    backgroundColor: "white",
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 15,
    height: 55,
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between"
    // zIndex: 999
  },
  contentDetails: {
    width: "100%",
    backgroundColor: "#ecf0f1",
    padding: 15
  },
  title: {
    fontSize: 16,
    marginTop: 22,
    fontWeight: "bold",
    color: "#2c3e50"
  },
  itemNombre: {
    fontSize: 17,
    fontWeight: "900"
  },
  priceProduct: {
    fontWeight: "bold",
    color: "black",
    fontSize: 24
  },
  visitas: {
    color: "#7f8c8d"
  },
  textStars: {
    paddingVertical: 5,
    alignItems: "center"
  },
  contentDescription: {
    marginVertical: 6,
    color: "#7f8c8d"
  },
  slideContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  infoImageProduct: {
    height: 300,
    marginTop: 0
  },
  imageContainer: {
    flex: 1,
    width: "100%",
    marginBottom: 1,
    marginTop: 0
  },
  imageProduct: {
    width: "100%",
    height: "100%"
  },
  slideContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  slide1: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  slide2: {
    backgroundColor: "rgba(20,200,20,0.3)"
  },
  slide3: {
    backgroundColor: "rgba(200,20,20,0.3)"
  },
  footer: {
    flexDirection: "row",
    elevation: 2,
    width: "100%",
    justifyContent: "space-between",
    backgroundColor: "#f7f7f7"
  },
  buttonLogin: {
    width: 300,
    backgroundColor: Colors.first,
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 8
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "900"
  }
});
