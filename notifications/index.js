import {Platform} from 'react-native';
import {Notifications} from 'react-native-notifications';

export const notificationInit = () => {
  Notifications.registerRemoteNotifications();

  Notifications.events().registerRemoteNotificationsRegistered(event => {
    // TODO: Send the token to my server so it could send back push notifications...
    console.warn('Device Token Received', event);
  });

  Notifications.events().registerNotificationReceivedForeground(
    (notification, completion) => {
      console.warn('Notification: ', notification);
      // Workaround for foreground notifications https://github.com/wix/react-native-notifications/issues/525
      if (Platform.OS === 'android') {
        const localNotification = {
          title: notification.payload['gcm.notification.title'],
          body: notification.payload['gcm.notification.body'],
        };
        const notificationId = notification.payload['google.sent_time'];
        Notifications.postLocalNotification(localNotification, notificationId);
      }
      completion({alert: true, sound: true, badge: false});
    },
  );

  Notifications.events().registerNotificationOpened(
    (notification, completion) => {
      console.warn('Notification was opened: ', notification);
      completion();
    },
  );
};
