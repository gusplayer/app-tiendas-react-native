import React, {Component} from 'react';
import Auth from './navigation/Auth';
import {Provider} from 'react-redux';
import createStore from './redux/store/store';
import {PersistGate} from 'redux-persist/integration/react';
import Icon from 'react-native-vector-icons/Feather';
import {notificationInit} from './notifications';

Icon.loadFont();
const {store, persistor} = createStore();

export default class App extends Component {
  componentDidMount() {
    notificationInit();
  }
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Auth />
        </PersistGate>
      </Provider>
    );
  }
}
